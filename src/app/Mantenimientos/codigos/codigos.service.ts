import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CodigosService {

  serverURL = environment.serverURL;
  constructor(private http: HttpClient) { }

  public get_codigos() {
    return this.http.get<any>(this.serverURL + 'manttos/codigos');
  }

  public put_codigos(data) {
    return this.http.put(this.serverURL + 'manttos/codigos', data);
  }

  public post_codigos(data) {
    return this.http.post(this.serverURL + 'manttos/codigos', data);
  }

  public get_imptos(codigo){
    return this.http.get<any>(this.serverURL + 'manttos/codimptos/' + codigo);
  }

  public get_desglose(codigo){
    return this.http.get<any>(this.serverURL + 'manttos/coddesg/' + codigo);
  }

  public get_contable(codigo){
    return this.http.get<any>(this.serverURL + 'manttos/codcont/' + codigo);
  }

}
