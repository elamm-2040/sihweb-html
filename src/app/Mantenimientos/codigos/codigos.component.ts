import { Component, OnInit } from '@angular/core';
import { CodigosService } from './codigos.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

interface Item {
  text: string
  value: string
}

@Component({
  selector: 'app-codigos',
  templateUrl: './codigos.component.html',
  styleUrls: ['./codigos.component.css']
})
export class CodigosComponent implements OnInit {
  ngFormCodigos: FormGroup;
  ngFormImptos: FormGroup;
  ngFormDesglose: FormGroup;
  ngFormContable: FormGroup;
  dataGrid: GridDataResult;
  codigoSelectedItem: string;
  TMSelectedItem: string
  gridCodigos = [];
  gridImptos = [];
  gridDesg = [];
  gridCont = [];
  ngFormSelection = [];
  manejo_manual = [];
  forma_pago = [];
  codigos: any;
  imptos: any;
  desglose: any;
  contable: any;
  pageSize = 10;
  skip = 0;
  codigoArr: Array<Item> = [
    { text: 'Seleccionar', value: '' },
    { text: 'Efectivo', value: '01' },
    { text: 'Cheque nominativo', value: '02' },
    { text: 'Transferencia electrónica de fondos', value: '03' },
    { text: 'Tarjeta de crédito', value: '04' },
    { text: 'Monedero electrónico', value: '05' },
    { text: 'Dinero electrónico', value: '06' },
    { text: 'Vales de despensa', value: '07' },
    { text: 'Dación en pago', value: '08' },
    { text: 'Pago por subrogación', value: '09' },
    { text: 'Pago por consignación', value: '10' },
    { text: 'Condonación', value: '11' },
    { text: 'Compensación', value: '12' },
    { text: 'Novación', value: '13' },
    { text: 'Confusión', value: '14' },
    { text: 'Remisión de deuda', value: '15' },
    { text: 'Prescripción o caducidad', value: '16' },
    { text: 'A satisfacción del acreedor', value: '17' },
    { text: 'Tarjeta de débito', value: '18' },
    { text: 'Tarjeta de servicios', value: '19' },
    { text: 'Aplicación de anticipos', value: '20' },
    { text: 'Por definir', value: '21' }
  ];

  constructor(private codigoServ: CodigosService,
    private formBuild: FormBuilder) {
    this.ngFormCodigos = this.createFormGroupCodigos();
    codigoServ.get_codigos().subscribe(res => {
      this.codigos = res.response.siCodigo.dsCodigos.ttCodigos;
      this.ngFormCodigos.patchValue(this.codigos[0]);
      this.fillManejoManual(this.codigos[0]);
      this.gridCodigos = this.codigos;
      this.ngFormSelection.push(this.gridCodigos[0].Codigo);
      this.codigoSelectedItem = this.codigos[0].cfdFP; //asigna el item seleccionado del JSON en el campo cfdFP

      this.ngFormCodigos.patchValue({ edit: true });

      this.ngFormImptos = this.createFormGroupImptos();
      this.consultarImpto();

      this.ngFormDesglose = this.createFormGroupDesglose();
      this.consultarDesglose();

      this.ngFormContable = this.createFormGroupContable();
      this.consultarContable();

      this.loadItems();
    });

  }

  ngOnInit() {
  }

  createFormGroupCodigos() {
    return this.formBuild.group({
      Codigo: [null],
      Con: [null],
      Cuenta2: [null],
      Inactivo: [null],
      Medida: [null],
      Num: [null],
      Tipo: [null],
      banco: [null],
      cErrDesc: [null],
      cImagen: [null],
      cRowID: [null],
      caux: [null],
      cclase: [null],
      ccmaximo: [null],
      ccomp1: [null],
      ccomp2: [null],
      ccredito1: [null],
      ccredito2: [null],
      ccuenta: [null],
      cdc: [null],
      cdebito1: [null],
      cdebito2: [null],
      cdesc: [null],
      cdesg: [null],
      cea: [null],
      cfdFP: [null],
      cfdPS: [null],
      cfp: [null],
      cmmanual: [null],
      cmoneda: [null],
      cnsubtotal: [null],
      coimpuesto: [null],
      coish: [null],
      cope: [null],
      cprop: [null],
      csecuencia: [null],
      ctr: [null],
      detalle: [null],
      dsecundaria: [null],
      iagrp: [null],
      lError: [null],
      edit: [null]
    });
  }

  createFormGroupImptos() {
    return this.formBuild.group({
      Codigo: [null],
      CodigoDesc: [null],
      Impto: [null],
      ImptoDesc: [null]
    });
  }

  createFormGroupDesglose() {
    return this.formBuild.group({
      Codigo: [null],
      CodigoDesc: [null],
      Detalle: [null],
      DetalleDesc: [null],
      Moneda: [null]
    });
  }

  createFormGroupContable() {
    return this.formBuild.group({
      Banco: [null],
      BancoDesc: [null],
      Codigo: [null],
      CodigoDesc: [null],
      CuentaAux: [null],
      Iagrp: [null],
      IngAgrpDesc: [null],
      cTM: [null],
      cuenta: [null]
    });
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.loadItems();
  }

  loadItems(): void {
    this.dataGrid = {
      data: this.gridCodigos.slice(this.skip, this.skip + this.pageSize),
      total: this.gridCodigos.length
    };
  }

  selectedCodigo(data) {
    this.ngFormCodigos.patchValue(data.selectedRows[0].dataItem);
    this.fillManejoManual(data.selectedRows[0].dataItem);
    this.codigoSelectedItem = data.selectedRows[0].dataItem.cfdFP;
    this.consultarImpto();
    this.consultarDesglose();
    this.consultarContable();

    this.ngFormCodigos.patchValue({ edit: true });
  }

  selectedImpto(data) {
    this.ngFormImptos.patchValue(data.selectedRows[0].dataItem);
  }

  selectedDesg(data) {
    this.ngFormDesglose.patchValue(data.selectedRows[0].datItem);
  }

  selectedCont(data) {
    this.ngFormContable.patchValue(data.selectedRows[0].datItem);
  }

  //Rellena los combo-box de manejo manual 
  fillManejoManual(data) {
    var manejo_manual_array = data.cmmanual.split("");
    this.forma_pago[data.cfp - 1] = true;
    for (let i = 0; i < manejo_manual_array.length; i++) {
      if (manejo_manual_array[i] === 'S') {
        this.manejo_manual[i] = true;
      } else {
        this.manejo_manual[i] = false;
      }
    }

  }

  consultarImpto() {
    this.codigoServ.get_imptos(this.ngFormCodigos.get('Codigo').value).subscribe(res => {
      this.imptos = res.response.siCodimpto.dsCodimptos.ttCodimptos;
      this.gridImptos = this.imptos;
      if (typeof this.imptos !== "undefined") {
        this.ngFormImptos.patchValue(this.imptos[0]);
      } else {
        this.ngFormImptos.reset();
      }
    });
  }

  consultarDesglose() {
    this.codigoServ.get_desglose(this.ngFormCodigos.get('Codigo').value).subscribe(res => {
      this.desglose = res.response.siCoddesg.dsCoddesg.ttCoddesg;
      this.gridDesg = this.desglose;
      if (typeof this.desglose !== "undefined") {
        this.ngFormDesglose.patchValue(this.desglose[0]);
        this.ngFormSelection.push(this.desglose.Codigo);
      } else {
        this.ngFormDesglose.reset();
      }
    });
  }

  consultarContable() {
    this.codigoServ.get_contable(this.ngFormCodigos.get('Codigo').value).subscribe(res => {
      this.contable = res.response.siCodcont.dsCodcont.ttCodcont;
      this.gridCont = this.contable;
      if (typeof this.contable !== "undefined") {
        this.ngFormContable.patchValue(this.contable[0]);
      } else {
        this.ngFormContable.reset();
      }
    });
  }

  nuevo() {
    this.ngFormCodigos.reset();
    for (let i = 0; i < this.manejo_manual.length; i++) {
      this.manejo_manual[i] = false;
    }
    this.ngFormSelection = [];

    this.ngFormCodigos.patchValue({ edit: false });
  }

  guardar() {

    let str = '';
    for (let i = 0; i < this.manejo_manual.length; i++) {
      if (this.manejo_manual[i]) {
        str += 'S';
      } else {
        str += 'N';
      }
    }
    this.ngFormCodigos.patchValue({ cmmanual: str });
    console.log(this.ngFormCodigos.value);

    if (this.ngFormCodigos.value.edit) {
      console.log('put');
      this.codigoServ.put_codigos(this.ngFormCodigos.value).subscribe(res =>{
        console.log(res);
      });
    } else {
      console.log('post');
    }
  }

}
