import { Component, OnInit } from '@angular/core';
import { MedidasService } from './medidas.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-medidas',
  templateUrl: './medidas.component.html',
  styleUrls: ['./medidas.component.css']
})
export class MedidasComponent implements OnInit {
  ngForm: FormGroup;
  ngFormOpc: FormGroup;
  gridMedidas = [];
  medidas: any;
  opc: any;

  constructor(private medidaServ: MedidasService,
    private formBuild: FormBuilder) {

    this.ngForm = this.createFormGroup();
    this.ngFormOpc = this.createFormGroupOpc();
    medidaServ.get_unidades_medida().subscribe(res => {
      this.medidas = res.response.siMedida.dsMedidas.ttMedidas;
      this.gridMedidas = this.medidas;
      this.ngForm.patchValue(this.medidas[0]);

      medidaServ.get_opcion_medidas(this.ngForm.get('cMedida').value).subscribe(res => {
        console.log(res);
        this.opc = res.response.siMedida.dsMedidas.ttMedidas;
        this.ngFormOpc.patchValue(this.opc[0])
      });
    });
  }

  ngOnInit() {
  }

  public createFormGroup() {
    return this.formBuild.group({
      Inactivo: [null],
      cDesc: [null],
      cMedida: [null],
      cfdMedida: [null]
    });
  }

  public createFormGroupOpc() {
    return this.formBuild.group({
      Inactivo: [null],
      cDesc: [null],
      cMedida: [null],
      cfdMedida: [null]
    });
  }

  guardarMedida() {

  }

  modificarMedida() {
    this.ngFormOpc.reset();
  }

  eliminarMedida() {

  }

  selectedMedida(data) {
    this.ngFormOpc.patchValue(data.selectedRows[0].dataItem);
  }

}
