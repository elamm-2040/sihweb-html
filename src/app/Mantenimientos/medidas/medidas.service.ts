import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedidasService {

  serverURL = environment.serverURL;

  constructor(private http: HttpClient) { }

  public get_unidades_medida() {
    return this.http.get<any>(this.serverURL + 'manttos/medidas');
  }

  public get_opcion_medidas(opcion) {
    return this.http.get<any>(this.serverURL + 'manttos/medidas/' + opcion);
  }

  public modificar_unidades_medida(data){
    return this.http.put<any>(this.serverURL + 'manttos/medidas', data);
  }

  public modificar_opcion_medidas(opcion, data){
    return this.http.put<any>(this.serverURL + 'manttos/medidas/' + opcion, data);
  }

  public guardar_unidades_medida(data){
    return this.http.post<any>(this.serverURL + 'manttos/medidas', data);
  }

  public guardar_opcion_medidas(opcion, data){
    return this.http.post<any>(this.serverURL + 'manttos/medidas/' + opcion, data);
  }

  public eliminar_unidades_medida(data){
    return this.http.delete<any>(this.serverURL + 'manttos/medidas', data);
  }

  public eliminar_opcion_medidas(opcion, data){
    return this.http.delete<any>(this.serverURL + 'manttos/medidas/' + opcion, data);
  }

}
