import { Component, OnInit } from '@angular/core';
import { EmpresasService } from './empresas.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-empresas',
    templateUrl: './empresas.component.html',
    styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {
    public gridData: any[] = [];
    gridDataTarifas = [];
    gridDataRFC = [];
    private data: object[];
    dataGrid: GridDataResult;
    pageSize = 10;
    skip = 0;
    empresas: any;
    tarifas: any;
    ngForm: FormGroup;
    tarifa = ({
        prov: '',
        Empresa: '',
        atarifa: '',
        atipo: ''
    });
    rfc =({
        Empresa: '',
        rfc: ''
    });

    constructor(private empServ: EmpresasService,
        private formBuild: FormBuilder,
        private modalService: NgbModal) {

        this.ngForm = this.createFormGroup();
        this.empServ.get_empresas().subscribe(res => {
            this.empresas = res.response.siEmpresa.dsEmpresas.ttEmpresas;
            console.log(this.empresas);
            this.ngForm.patchValue(this.empresas[0]);
            this.gridData = this.empresas;
            this.loadItems();
        });
    }

    ngOnInit() {
    }

    createFormGroup() {
        return this.formBuild.group({
            Empresa: [null],
            enombre: [null],
            edireccion1: [null],
            edireccion2: [null],
            eciudad: [null],
            estado: [null],
            ecp: [null],
            etelefono1: [null],
            etelefono2: [null],
            efax: [null],
            rfc: [null],
            Clasif: [null],
            etipo: [null],
            treser: [null],
            Segmento: [null],
            efp: [null],
            Origen: [null],
            Sucursal: [null],
            Cobranza: [null],
            efact: [null],
            erfact: [null],
            eenv: [null],
            Vendedor: [null],
            enotas: [null],
            Usuario: [null],
            Inactivo: [null],
            econtacto1: [null],
            econtacto2: [null],
            econtacto3: [null],
            econtacto4: [null],
            econtacto5: [null],
            econtacto6: [null],
            epuesto1: [null],
            epuesto2: [null],
            epuesto3: [null],
            epuesto4: [null],
            epuesto5: [null],
            epuesto6: [null],
            eemail1: [null],
            eemail2: [null],
            eemail3: [null],
            eemail4: [null],
            eemail5: [null],
            eemail6: [null]
        });
    }

    selectedTarifa(data){
        console.log(data);
        this.tarifa.prov = data.selectedRows[0].dataItem.prov;
        this.tarifa.Empresa = data.selectedRows[0].dataItem.Empresa;
        this.tarifa.atarifa = data.selectedRows[0].dataItem.atarifa;
        this.tarifa.atipo = data.selectedRows[0].dataItem.atipo;
    }

    selectedRFC(data){
        
    }

    selectedEmpresa(data) {
        console.log(data);
        this.ngForm.patchValue(data.selectedRows[0].dataItem);
    }

    pageChange({ skip, take }: PageChangeEvent): void {
        this.skip = skip;
        this.pageSize = take;
        this.loadItems();
    }

    loadItems(): void {
        this.dataGrid = {
            data: this.gridData.slice(this.skip, this.skip + this.pageSize),
            total: this.gridData.length
        };
    }

    openModalTarifas(content) {
        this.modalService.open(content, {
            size: 'xl'
        });
        this.empServ.get_tarifas(this.ngForm.get('Empresa').value).subscribe(res => {
            this.tarifas = res.response.siEmpresa.dsEmpresas.ttEmptarifas;
            if (typeof this.tarifas !== "undefined") {
                console.log('entre');
                console.log(this.tarifas);
                this.gridDataTarifas = this.tarifas;
            }else{
                this.gridDataTarifas = [];
            }
        });
    }

    openModalRFC(content) {
        this.modalService.open(content, {
            size: 'xl'
        });
        this.empServ.get_RFC(this.ngForm.get('Empresa').value).subscribe(res => {
            this.rfc = res.response.siEmpresa.dsEmpresas.ttEmprfc;
            if (typeof this.rfc !== "undefined") {
                console.log('entre');
                console.log(this.rfc);
                //this.gridDataRFC = this.rfc;
            }else{
               //this.gridDataRFC = [];
            }
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }
}
