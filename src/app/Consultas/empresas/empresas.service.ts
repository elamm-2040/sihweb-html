import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {
  serverURL = environment.serverURL;

  constructor(private http: HttpClient) {
  }

  public get_empresas() {
    return this.http.get<any>(this.serverURL + 'manttos/empresas');
  }

  public get_tarifas(empresa_name) {
    return this.http.get<any>(this.serverURL + 'manttos/empresasbotones/T®' + empresa_name);
  }

  public get_RFC(empresa_name) {
    return this.http.get<any>(this.serverURL + 'manttos/empresasbotones/R®' + empresa_name);
  }
}
