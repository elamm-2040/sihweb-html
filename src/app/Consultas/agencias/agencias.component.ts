import { Component, OnInit } from '@angular/core';
import { AgenciasService } from './agencias.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-agencias',
  templateUrl: './agencias.component.html',
  styleUrls: ['./agencias.component.css']
})
export class AgenciasComponent implements OnInit {

  gridData = [];
  gridDataServicios = [];
  comisiones = [];
  agencias: any;
  servicios: any;
  ngForm: FormGroup;
  ngFormServicios: FormGroup;
  dataGrid: GridDataResult;
  pageSize = 10;
  skip = 0;

  comision_array: any = {
    Agencia: '',
    proc: '',
    tarifa: '',
    atipo: '',
    acomision: '',
    amonto: ''
  };

  tarifa_array: any = {
    prov: '',
    Agencia: '',
    atarifa: '',
    atipo: ''
  };

  allotmentArr: any = {
    Agencia: '',
    msecuencia: '',
    tcuarto: '',
    mfecini: '',
    mfecter: '',
    mnctos: '',
    mcutoff: '',
    mhotel: '',
    magencia: '',
    mnotas: '',
    Usuario: ''
  };

  central_array: any = {
    Agencia: '',
    Estado: '',
    aciudad: '',
    acp: '',
    adesc: '',
    adireccion: [null],
    aemail: '',
    afax: '',
    atel0: '',
    atel1: '',
    adir0: '',
    adir1: ''
  };

  garantia_array: any = {
    Agencia: '',
    msecuencia: '',
    mfecini: '',
    mfecter: '',
    Usuario: '',
    tcuarto: '',
    mnctos: '',
    mcutoff: ''
  };

  constructor(private agenciaServ: AgenciasService,
    private formBuild: FormBuilder,
    private modalService: NgbModal) {
    this.ngForm = this.createFormGroup();
    this.agenciaServ.get_agencias().subscribe(res => {
      this.agencias = res.response.siAyuda.dsAyuda.ttAgencias;
      this.ngForm.patchValue(this.agencias[0]);
      this.gridData = this.agencias;

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'C').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgcomisiones;
        if (typeof this.servicios !== "undefined") {
          this.fillComision(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'T').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgtarifas;
        if (typeof this.servicios !== "undefined") {
          this.fillTarifa(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'A').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAllotma;
        if (typeof this.servicios !== "undefined") {
          this.fillAllotment(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'E').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgcentral;
        if (typeof this.servicios !== "undefined") {
          this.fillCentral(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'M').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgmistery;
        if (typeof this.servicios !== "undefined") {
          this.fillMistery(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'O').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgobserv;
        if (typeof this.servicios !== "undefined") {
          this.fillObservaciones(this.servicios[0]);
        }
      });

      this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, 'G').subscribe(res => {
        this.servicios = res.response.siAgencia.dsAgencias.ttAggarantia;
        if (typeof this.servicios !== "undefined") {
          this.fillGarantia(this.servicios[0]);
        }
      });

      this.loadItems();

    });
  }

  ngOnInit() {
  }

  createFormGroup() {
    return this.formBuild.group({
      Agencia: [null],
      anombre: [null],
      adireccion1: [null],
      adireccion2: [null],
      aciudad: [null],
      estado: [null],
      acp: [null],
      atelefono1: [null],
      atelefono2: [null],
      afax: [null],
      aemail: [null],
      rfc: [null],
      atipo: [null],
      aclasif: [null],
      atop: [null],
      acontrol: [null],
      segmento: [null],
      treser: [null],
      afp: [null],
      origen: [null],
      aiata: [null],
      sucursal: [null],
      cobranza: [null],
      afact: [null],
      arfact: [null],
      //forma fact no encontrada
      Vendedor: [null],
      Maestra: [null],
      anotas: [null],
      Usuario: [null],
      Inactivo: [null],
      acontacto1: [null],
      acontacto2: [null],
      acontacto3: [null],
      acontacto4: [null],
      acontacto5: [null],
      acontacto6: [null],
      apuesto1: [null],
      apuesto2: [null],
      apuesto3: [null],
      apuesto4: [null],
      apuesto5: [null],
      apuesto6: [null],
      aemc1: [null],
      aemc2: [null],
      aemc3: [null],
      aemc4: [null],
      aemc5: [null],
      aemc6: [null],


    });
  }

  selectedAgencia(data) {
    this.ngForm.patchValue(data.selectedRows[0].dataItem);
  }

  selectedComision(data) {
    this.fillComision(data.selectedRows[0].dataItem);
  }

  selectedTarifa(data) {
    this.fillTarifa(data.selectedRows[0].dataItem);
  }

  selectedAllotment(data) {
    this.fillAllotment(data.selectedRows[0].dataItem);
  }

  selectedCentrales(data) {
    this.fillCentral(data.selectedRows[0].dataItem);
  }

  selectedGarantia(data) {
    this.fillGarantia(data.selectedRows[0].dataItem);
  }

  pageChange({ skip, take }: PageChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.loadItems();
  }

  loadItems(): void {
    this.dataGrid = {
      data: this.gridData.slice(this.skip, this.skip + this.pageSize),
      total: this.gridData.length
    };
  }

  openModal(content, opcion) {
    this.modalService.open(content, {
      size: 'xl'
    });

    this.agenciaServ.get_servicio(this.ngForm.get('Agencia').value, opcion).subscribe(res => {
      if (opcion === 'C') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgcomisiones;
        if (typeof this.servicios !== "undefined") {
          this.fillComision(this.servicios[0]);
        }
      }
      if (opcion === 'T') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgtarifas;
        if (typeof this.servicios !== "undefined") {
          this.fillTarifa(this.servicios[0]);
        }
      }
      if (opcion === 'A') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAllotma;
        if (typeof this.servicios !== "undefined") {
          this.fillAllotment(this.servicios[0]);
        }
      }
      if (opcion === 'E') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgcentral;
        if (typeof this.servicios !== "undefined") {
          this.fillCentral(this.servicios[0]);
        }
      }
      if (opcion === 'M') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgmistery;
        if (typeof this.servicios !== "undefined") {
          this.fillMistery(this.servicios[0]);
        }
      }
      if (opcion === 'O') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAgobserv;
        if (typeof this.servicios !== "undefined") {
          this.fillObservaciones(this.servicios[0]);
        }
      }
      if (opcion === 'G') {
        this.servicios = res.response.siAgencia.dsAgencias.ttAggarantia;
        if (typeof this.servicios !== "undefined") {
          this.fillGarantia(this.servicios[0]);
        }
      }
      if (typeof this.servicios !== "undefined") {
        this.gridDataServicios = this.servicios;
        /*  this.ngFormServicios.patchValue(this.servicios[0]); */
      } else {
        this.gridDataServicios = [];
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  fillComision(data) {
    this.comision_array.Agencia = data.Agencia;
    this.comision_array.proc = data.proc;
    this.comision_array.tarifa = data.tarifa;
    this.comision_array.atipo = data.atipo
    this.comision_array.acomision = data.acomision;
    this.comision_array.amonto = data.amonto;

    var tmp = this.comision_array.acomision.split("");
    var i;
    for (i = 0; i < tmp.length; i++) {
      if (tmp[i] === 'S') {
        this.comisiones[i] = true;
      } else {
        this.comisiones[i] = false;
      }
    }
  }

  fillTarifa(ttAgtarifas: any) {
    this.tarifa_array.prov = ttAgtarifas.prov;
    this.tarifa_array.Agencia = ttAgtarifas.Agencia;
    this.tarifa_array.atarifa = ttAgtarifas.atarifa;
    this.tarifa_array.atipo = ttAgtarifas.atipo;
  }

  fillAllotment(ttAllotma: any) {
    this.allotmentArr.Agencia = ttAllotma.Agencia;
    this.allotmentArr.msecuencia = ttAllotma.msecuencia;
    this.allotmentArr.tcuarto = ttAllotma.tcuarto;
    this.allotmentArr.mfecini = ttAllotma.mfecini;
    this.allotmentArr.mfecter = ttAllotma.mfecter;
    this.allotmentArr.mnctos = ttAllotma.mnctos;
    this.allotmentArr.mcutoff = ttAllotma.mcutoff;
    this.allotmentArr.mhotel = ttAllotma.mhotel;
    this.allotmentArr.magencia = ttAllotma.magencia;
    this.allotmentArr.mnotas = ttAllotma.mnotas;
    this.allotmentArr.Usuario = ttAllotma.Usuario;
  }

  fillGarantia(ttAggarantia: any) {
    this.garantia_array.Agencia = ttAggarantia.Agencia;
    this.garantia_array.Usuario = ttAggarantia.Usuario;
    this.garantia_array.mnctos = ttAggarantia.mnctos;
    this.garantia_array.tcuarto = ttAggarantia.tcuarto;
    this.garantia_array.msecuencia = ttAggarantia.msecuencia;
    this.garantia_array.mfecini = ttAggarantia.mfecini;
    this.garantia_array.mfecter = ttAggarantia.mfecter;
    this.garantia_array.mcutoff = ttAggarantia.mcutoff;
  }

  fillObservaciones(ttAgobserv: any) {

  }

  fillMistery(ttAgmistery: any) {

  }

  fillCentral(ttAgcentral: any) {
    this.central_array.Agencia = ttAgcentral.Agencia;
    this.central_array.adesc = ttAgcentral.adesc;
    this.central_array.atel0 = ttAgcentral.atelefono[0];
    this.central_array.atel1 = ttAgcentral.atelefono[1];
    this.central_array.adir0 = ttAgcentral.adireccion[0];
    this.central_array.adir1 = ttAgcentral.adireccion[1];
    this.central_array.afax = ttAgcentral.afax;
    this.central_array.aciudad = ttAgcentral.aciudad;
    this.central_array.Estado = ttAgcentral.Estado;
    this.central_array.acp = ttAgcentral.acp;
    this.central_array.aemail = ttAgcentral.aemail;
  }

}
