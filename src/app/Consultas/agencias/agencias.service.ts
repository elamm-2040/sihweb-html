import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AgenciasService {
  serverURL = environment.serverURL;

  constructor(private http: HttpClient) {
  }

  public get_agencias() {
    return this.http.get<any>(this.serverURL + 'ayuda/agencias');
  }

  public get_servicio(agencia_name, opcion) {
    return this.http.get<any>(this.serverURL + 'manttos/agenbotones/' + opcion + '®' + agencia_name);
  }

}
