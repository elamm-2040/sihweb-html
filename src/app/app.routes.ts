import {RouterModule, Routes} from '@angular/router';
import {EmpresasComponent} from './Consultas/empresas/empresas.component';
import {BaseLayoutComponent} from './_architect/Layout/base-layout/base-layout.component';
import {AgenciasComponent} from './Consultas/agencias/agencias.component';
import {LandingComponent} from './components/landing/landing.component';
import { TiposCambioComponent } from './Mantenimientos/tipos-cambio/tipos-cambio.component';
import { ParametrosComponent } from './Parametros/parametros/parametros.component';
import { MedidasComponent } from './Mantenimientos/medidas/medidas.component';
import { CodigosComponent } from './Mantenimientos/codigos/codigos.component';


const APP_ROUTES: Routes = [
  {
    path: '',
    component: BaseLayoutComponent,
    children: [
      {path: '', component: LandingComponent, data: {extraParameter: ''}},
      {path: 'empresas', component: EmpresasComponent, data: {extraParameter: ''}},
      {path: 'agencias', component: AgenciasComponent, data: {extraParameter: ''}},
      {path: 'tipos-cambio', component: TiposCambioComponent, data: {extraParameter: ''}},
      {path: 'parametros', component: ParametrosComponent, data: {extraParameter: ''}},
      {path: 'medidas', component: MedidasComponent, data: {extraParameter: ''}},
      {path: 'codigos', component: CodigosComponent, data: {extraParameter: ''}}
    ]
  }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
