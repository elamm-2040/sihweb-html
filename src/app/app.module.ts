import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {EmpresasComponent} from './Consultas/empresas/empresas.component';
import {APP_ROUTING} from './app.routes';
import {ArchitectModule} from './_architect/architect.module';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {GridModule} from '@progress/kendo-angular-grid';
import {InputsModule} from '@progress/kendo-angular-inputs';
import { AgenciasComponent } from './Consultas/agencias/agencias.component';
import { LandingComponent } from './components/landing/landing.component';
import { TiposCambioComponent } from './Mantenimientos/tipos-cambio/tipos-cambio.component';
import { ParametrosComponent } from './Parametros/parametros/parametros.component';
import { MedidasComponent } from './Mantenimientos/medidas/medidas.component';
import { CodigosComponent } from './Mantenimientos/codigos/codigos.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';

@NgModule({
    declarations: [
        AppComponent,
        EmpresasComponent,
        AgenciasComponent,
        LandingComponent,
        TiposCambioComponent,
        ParametrosComponent,
        MedidasComponent,
        CodigosComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        AppRoutingModule,
        APP_ROUTING,
        ArchitectModule,
        ReactiveFormsModule,
        GridModule,
        InputsModule,
        DropDownsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
