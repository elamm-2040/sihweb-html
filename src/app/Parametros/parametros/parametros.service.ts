import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ParametrosService {

  serverURL = environment.serverURL;

  constructor(private http: HttpClient) {
   }

  public get_parametros(){
    return this.http.get<any>(this.serverURL + 'parametros/parametros/*');
  }

  public get_opcion_parametros(opcion){
    return this.http.get<any>(this.serverURL + 'parametros/parametros/' + opcion);
  }
}
