import { Component, OnInit } from '@angular/core';
import { ParametrosService } from './parametros.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-parametros',
  templateUrl: './parametros.component.html',
  styleUrls: ['./parametros.component.css']
})
export class ParametrosComponent implements OnInit {
  auditoriaFlag = false;
  facturasFlag = false;
  formatoGeneralFlag = false;
  llavesCreditosFlag = false;
  chequesFlag = false;
  parametros: any;
  opciones: any;
  gridParametros = [];
  ngFormAuditorias: FormGroup;
  ngFormFacturas: FormGroup;
  ngFormFormatoGral: FormGroup;
  ngFormLlavesCreditos: FormGroup;
  ngFormCheques: FormGroup;

  constructor(private ParamService: ParametrosService,
    private formBuild: FormBuilder) {
    ParamService.get_parametros().subscribe(res => {
      this.parametros = res.response.siParametro.dsParametros.ttParametros;
      this.gridParametros = this.parametros;

      this.ngFormAuditorias = this.createFormGroupAuditorias();
      ParamService.get_opcion_parametros('AUDITORIA-NOCTURNA').subscribe(res => {
        this.opciones = res.response.siParametro.dsParametros.ttParametros;
        this.ngFormAuditorias.patchValue(this.opciones[0]);
      });

      this.ngFormFacturas = this.createFormGroupFacturas();
      ParamService.get_opcion_parametros('FACTURAS').subscribe(res => {
        this.opciones = res.response.siParametro.dsParametros.ttParametros;
        this.ngFormFacturas.patchValue(this.opciones[0]);
      });

      this.ngFormFormatoGral = this.createFormGroupFormatoGral();
      ParamService.get_opcion_parametros('FORMATO-GENERAL').subscribe(res => {
        this.opciones = res.response.siParametro.dsParametros.ttParametros;
        this.ngFormFormatoGral.patchValue(this.opciones[0]);
      });

      this.ngFormLlavesCreditos = this.createFormGroupLlavesCreditos();
      ParamService.get_opcion_parametros('LLAVES-CREDITOS').subscribe(res => {
        this.opciones = res.response.siParametro.dsParametros.ttParametros;
        this.ngFormLlavesCreditos.patchValue(this.opciones[0]);
      });

      this.ngFormCheques = this.createFormGroupCheques();
      ParamService.get_opcion_parametros('PV-CHEQUES').subscribe(res => {
        this.opciones = res.response.siParametro.dsParametros.ttParametros;
        this.ngFormCheques.patchValue(this.opciones[0]);
      });

    });
  }

  ngOnInit() {
  }

  createFormGroupAuditorias() {
    return this.formBuild.group({
      parametro: [null],
      descripcion: [null],
      Borrar: [null],
      pchr1: [null],
      pchr2: [null],
      pchr3: [null],
      pchr4: [null],
      pchr5: [null],
      pchr6: [null],
      pchr7: [null],
      pchr8: [null],
      pchr9: [null],
      pchr10: [null],
      pchr11: [null],
      pchr12: [null],
      pchr13: [null],
      pchr14: [null],
      pchr20: [null]
    });
  }

  createFormGroupFacturas() {
    return this.formBuild.group({
      parametro: [null],
      descripcion: [null],
      Borrar: [null],
      pchr1: [null],
      pchr2: [null],
      pchr3: [null],
      pchr4: [null],
      pchr5: [null],
      pchr6: [null],
      pchr7: [null],
      pchr8: [null],
      pchr9: [null],
      pchr10: [null],
      pchr11: [null],
      pchr12: [null],
      pchr13: [null],
      pchr14: [null],
      pchr15: [null],
      pchr16: [null],
      pchr17: [null],
      pchr18: [null],
      pchr19: [null],
      pchr20: [null]
    });
  }

  createFormGroupFormatoGral() {
    return this.formBuild.group({
      parametro: [null],
      descripcion: [null],
      Borrar: [null],
      pchr1: [null],
    });
  }

  createFormGroupLlavesCreditos() {
    return this.formBuild.group({
      parametro: [null],
      descripcion: [null],
      pchr1: [null]
    });
  }

  createFormGroupCheques() {
    return this.formBuild.group({
      parametro: [null],
      descripcion: [null],
      pchr1: [null],
      pchr2: [null],
      pchr3: [null],
      pchr4: [null],
      pchr5: [null],
      pchr6: [null],
      pchr7: [null],
      pchr8: [null],
      pchr9: [null],
      pchr10: [null]
    });
  }

  selectedParametro(data) {
    this.auditoriaFlag = false;
    this.facturasFlag = false;
    this.formatoGeneralFlag = false;
    this.llavesCreditosFlag = false;
    this.chequesFlag = false;
    if (data.selectedRows[0].dataItem.parametro === "AUDITORIA-NOCTURNA") {
      this.auditoriaFlag = true;
    }
    if (data.selectedRows[0].dataItem.parametro === "FACTURAS") {
      this.facturasFlag = true;
    }
    if (data.selectedRows[0].dataItem.parametro === "FORMATO-GENERAL") {
      this.formatoGeneralFlag = true;
    }
    if (data.selectedRows[0].dataItem.parametro === "LLAVES-CREDITOS") {
      this.llavesCreditosFlag = true;
    }
    if (data.selectedRows[0].dataItem.parametro === "PV-CHEQUES") {
      this.chequesFlag = true;
    }
  }

}
